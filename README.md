# NestJS Application

This application is a basic one taken from [official examples](https://github.com/nestjs/nest/tree/master/sample/01-cats-app).
It is only here to demonstrate an fully automated deployment of a complete application & infrastructure.

To know more about this project, please refer to [ops](https://gitlab.com/kube6/ops) repository.

## Routes 

| Route     | Method | Description | 
| --------- | ------ | ------------------------------------------------ | 
| /         | GET    | Show the name of footballer who goaled in May 93 | 
| /cats     | GET    | Show cats array                                  | 
| /health   | GET    | Health check route                               | 

## CI/CD

This project pipeline is composed of three jobs which are:

```
    npm_install     --- Taken from R2Devops.io, install npm packages  
    npm_build       --- Taken from R2Devops.io, build project
    deploy_trigger  --- Call an HTTP API Route to trigger deployment pipeline in Ops project
```

As you noticed, some of the jobs are taken from [R2Devops](https://r2devops.io) which is a  GitLab job registry where jobs are plug & play.

## License

This application is [Apache2 Licensed](LICENSE).