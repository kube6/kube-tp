import {Controller, Get, UseGuards} from "@nestjs/common";
import { HealthCheck, HealthCheckService, HttpHealthIndicator } from "@nestjs/terminus";
import {RolesGuard} from "./common/guards/roles.guard";

@UseGuards(RolesGuard)
@Controller('')
export class HomeController {
	constructor(
		private health: HealthCheckService,
		private http: HttpHealthIndicator,
	) {}

	@Get('/health')
	@HealthCheck()
	check() {
		return this.health.check([
			() => this.http.pingCheck('google', 'https://google.com'),
		]);
	}

	@Get('')
	home() {
		return {"message": "C'était le 26 mai 93, par " + process.env.BUTEUR}
	}
}
