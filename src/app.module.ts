import { Module } from '@nestjs/common';
import { CatsModule } from './cats/cats.module';
import { CoreModule } from './core/core.module';
import {HomeController} from "./home.controller";
import {LoggerModule} from "nestjs-pino";
import {PrometheusModule} from "@willsoto/nestjs-prometheus";
import { TerminusModule } from '@nestjs/terminus';

@Module({
  controllers: [HomeController],
  imports: [
  	CoreModule,
	CatsModule,
	PrometheusModule.register(),
	LoggerModule.forRoot(),
	TerminusModule,
  ],
})
export class AppModule {}
