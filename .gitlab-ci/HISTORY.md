# Commands history

```shell
kubectl create ns app
kubectl apply -f .gitlab-ci/service-account.yml
kubectl apply -f .gitlab-ci/role.yml
```

```shell
kubectl secret --namespace=app

➜ kubectl get secret --namespace=app
NAME                  TYPE                                  DATA   AGE
app-sa-token-stn*l    kubernetes.io/service-account-token   3      5m57s
default-token-8zmlm   kubernetes.io/service-account-token   3      37m
```

```shell
kubectl get secret app-sa-token-stn*l --namespace=app -o yaml

apiVersion: v1
data:
  ca.crt: OMITTED
  namespace: YXBw
  token: OMITTED
kind: Secret
metadata:
  annotations:
    kubernetes.io/service-account.name: app-sa
    kubernetes.io/service-account.uid: OMITTED
  creationTimestamp: "2021-07-16T07:01:14Z"
  name: app-sa-token-stn*l
  namespace: app
  resourceVersion: "14127130899"
  uid: OMITTED
type: kubernetes.io/service-account-token


The token is in base64, so we need to decode it properly.
```

```shell
# Install the ingress controller
 kubectl apply -f .gitlab-ci/05-ingress.yml

# Verify installation
kubectl get pods -n ingress-nginx \
  -l app.kubernetes.io/name=ingress-nginx --watch
```

